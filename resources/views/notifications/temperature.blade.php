@component('mail::layout')

    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            HomeAutomation
        @endcomponent
    @endslot

    {{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Cześć, {{ $name }}!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
Pozdrawiamy,<br>HomeAutomation
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Jeśli masz problem z kliknięciem w przycisk "{{ $actionText }}", skopiuj i wklej
poniższy link do paska przeglądarki: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            HomeAutomation 2018.
        @endcomponent
    @endslot
@endcomponent
