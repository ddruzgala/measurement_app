@extends('adminlte::page')

@section('contentheader_title')
    Czujniki
@endsection

@section('htmlheader_title')
	Czujniki
@endsection

<style>
    @media (max-width: 1200px) {
        .open-edit {
            margin-bottom: 10px !important;
        }
    }
</style>

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Czujniki</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <table id="sensors" class="table table-hover">
                                <thead>
                                <div class="text-right">
                                    <a href="#" data-toggle="modal" data-target="#modal-add" class="btn btn-success">Dodaj</a>

                                </div>

                                <tr>
                                    <th class="col-lg-2 col-xs-2">Id</th>
                                    <th class="col-lg-2 col-xs-4">Nazwa</th>
                                    <th class="col-lg-2 col-xs-4">Pomieszczenie</th>
                                    <th class="col-lg-2 col-xs-2">Akcja</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sensors as $sensor)
                                    <tr>
                                        <td>{{$sensor->id}}</td>
                                        <td>{{$sensor->name}}</td>
                                        <td>
                                            @if(!empty($sensor->room))
                                                <a href="{{url('/room/'.$sensor->room->id)}}">
                                                    {{$sensor->room->name}}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#modal-edit"
                                               class="btn btn-primary open-edit"
                                               data-id="{{$sensor->id}}"
                                               data-name="{{$sensor->name}}"
                                            >
                                                Edycja
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#modal-delete"
                                               class="btn btn-danger open-delete"
                                               data-id="{{$sensor->id}}"
                                               data-name="{{$sensor->name}}"
                                            >
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Nazwa</th>
                                    <th>Sensor</th>
                                    <th>Akcja</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="modal modal-info fade" id="modal-edit">
                            <div class="modal-dialog">
                                <form role="form" id="editForm" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Edycja</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="name" class="form-control" placeholder="Nazwa" id="sensorName">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Anuluj</button>
                                            <button type="submit" class="btn btn-outline">Zapisz</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </form>
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <div class="modal modal-success fade" id="modal-add">
                            <div class="modal-dialog">
                                <form role="form" id="addForm" action="{{url('/sensor')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Dodaj nowy czujnik</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="name" class="form-control" placeholder="Nazwa" id="sensorName">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Anuluj</button>
                                            <button type="submit" class="btn btn-outline">Dodaj</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </form>
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <div class="modal modal-danger fade" id="modal-delete">
                            <div class="modal-dialog">
                                <form role="form" id="deleteForm" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Delete</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p id="deleteMsg"></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Anuluj</button>
                                            <button type="submit" class="btn btn-outline">Usuń</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </form>
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('#sensors').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : false,
                'info'        : true,
                'autoWidth'   : false
            })
        })

        $(document).on('click','.open-edit',function(){
            var id = $(this).data('id');
            var name = $(this).data('name');
            $('#sensorName').val(name);
            $('#editForm').attr('action', APP_URL + '/sensor/' + id + '/update');
        });

        $(document).on('click','.open-delete',function(){
            var id = $(this).data('id');
            var name = $(this).data('name');
            $('#deleteMsg').html('Czy na pewno chcesz usunąć ' + name + '?');
            $('#deleteForm').attr('action', APP_URL + '/sensor/' + id + '/delete');
        });
    </script>
@endsection