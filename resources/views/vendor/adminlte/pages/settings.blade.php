@extends('adminlte::page')

@section('contentheader_title')
    Ustawienia
@endsection

@section('htmlheader_title')
	Ustawienia
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Aktywacja pomieszczeń</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group" id="rooms">
                            <div class="row">
                                <div class="col-lg-6 col-xs-6">
                                    <h4>Pomieszczenie</h4>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <h4>Aktywne</h4>
                                </div>
                            </div>
                            @foreach($rooms as $room)
                                <div class="row">
                                    <div class="col-lg-6 col-xs-6">
                                        {{$room->name}}
                                    </div>
                                    <div class="col-lg-6 col-xs-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="" class="room" id="{{$room->id}}"
                                                       @if($room->active) checked @endif
                                                        onclick="activateRooms(this)"
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ustawienia powiadomień</h3>
                    </div>
                    <div class="box-body">
                        <h4>Aktywuj powiadomienia</h4>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <p>Włącz powiadomienia email</p>
                            </div>
                            <!-- text input -->
                            <div class="form-group col-lg-6 col-xs-6">
                                <label>
                                    <input type="checkbox" name="email"
                                           @if($preferences->email) checked @endif
                                           onclick="updatePreferences()">
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <p>Włącz powiadomienia sms</p>
                            </div>
                            <!-- text input -->
                            <div class="form-group col-lg-6 col-xs-6">
                                <label>
                                    <input type="checkbox" name="sms"
                                           @if($preferences->sms) checked @endif
                                           onclick="updatePreferences()">
                                </label>
                            </div>
                        </div>
                        <br>
                        <h4>Limity temperatur</h4>
                        <hr>
                        <div class="row">
                            <!-- text input -->
                            <div class="form-group col-lg-6 col-xs-12">
                                <label>Temperatura minimalna</label>
                                <input type="number" class="form-control"
                                       name="temp_min" value="{{ $tempMin }}"
                                       onchange="setLimits()">
                            </div>
                            <!-- text input -->
                            <div class="form-group col-lg-6 col-xs-12">
                                <label>Temperatura maksymalna</label>
                                <input type="number" class="form-control"
                                       name="temp_max" value="{{ $tempMax }}"
                                       onchange="setLimits()">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        function activateRooms(room) {
            $.ajax({
                type: 'post',
                url: APP_URL + '/room/' + room.id + '/update',
                dataType: 'json',
                data: {
                    active: room.checked ? 1 : 0,
                    _method: 'PUT'
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(){
                    toastr.options.timeOut = 1500;
                    toastr.success('Pomieszczenia zaktualizowane!', 'Sukces');
                },
                error:function(data){
                    toastr.options.timeOut = 1500;
                    toastr.error('Spróbuj ponownie później', 'Błąd');
                }
            })
        }

        function setLimits() {
            var tempMin = $('input[name=temp_min]').val();
            var tempMax = $('input[name=temp_max]').val();

            $.ajax({
                type: 'post',
                url: APP_URL + '/user/' + {{\Illuminate\Support\Facades\Auth::user()->id}} + '/update',
                dataType: 'json',
                data: {
                    temp_min: tempMin,
                    temp_max: tempMax,
                    _method: 'PUT'
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    toastr.options.timeOut = 1500;
                    toastr.success(data.message, 'Sukces');
                },
                error:function(data){
                    toastr.options.timeOut = 1500;
                    if(data.status === 422) {
                        toastr.error(data.responseJSON.message, 'Błąd');
                    } else {
                        toastr.error('Spróbuj ponownie później.', 'Błąd');
                    }

                }
            })
        }

        function updatePreferences() {
            var email = $('input[name=email]').prop( "checked" );
            var sms = $('input[name=sms]').prop( "checked" );

            console.log('email', email);
            console.log('sms', sms);

            $.ajax({
                type: 'post',
                url: APP_URL + '/notification_preferences/' + {{ $preferences->id }} + '/update',
                dataType: 'json',
                data: {
                    email: email,
                    sms: sms,
                    _method: 'PUT'
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    toastr.options.timeOut = 1500;
                    toastr.success(data.message, 'Sukces');
                },
                error:function(data){
                    console.log(data)
                    toastr.options.timeOut = 1500;
                    toastr.error('Spróbuj ponownie później.', 'Błąd');

                }
            })
        }
    </script>
@endsection
