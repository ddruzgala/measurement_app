@extends('adminlte::page')

@section('contentheader_title')
    Pomiary
@endsection

@section('htmlheader_title')
	Pomiary
@endsection

<style>
    .date-th {
        min-width: 90px;
    }
</style>

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Wybierz przedział czasowy</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <form action="{{ url('/measurement') }}" method="get">
                                <div class="form-group">
                                    <div class="input-group">
                                        <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> <span>Przedział czasowy</span> <i class="fa fa-caret-down"></i>
                                        </button>
                                        <input type="hidden" id="dateRange" name="date_range" value="">
                                    </div>
                                </div>
                                <button type="submit" id="btn" class="btn btn-primary">Wyświetl</button>
                            </form>
                            <!-- /.form group -->
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pomiary {{ $range }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <table id="measurements" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        <span class="hidden-sm hidden-xs">Temperatura [°C]</span>
                                        <span class="visible-sm visible-xs">T [°C]</span>
                                    </th>
                                    <th>
                                        <span class="hidden-sm hidden-xs">Wilgotność [%]</span>
                                        <span class="visible-sm visible-xs">W [%]</span>
                                    </th>
                                    <th class="hidden-sm hidden-xs">Czujnik</th>
                                    <th>Pomieszczenie</th>
                                    <th class="date-th">Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($measurements as $measurement)
                                    <tr>
                                        <td>{{$measurement->temperature}}</td>
                                        <td>{{$measurement->humidity}}</td>
                                        <td class="hidden-sm hidden-xs">{{$measurement->sensor->name}}</td>
                                        <td>{{$measurement->sensor->room->name}}</td>
                                        <td>{{\Carbon\Carbon::parse($measurement->created_at)->addHour()->toDateTimeString()}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>
                                        <span class="hidden-sm hidden-xs">Temperatura [°C]</span>
                                        <span class="visible-sm visible-xs">T [°C]</span>
                                    </th>
                                    <th>
                                        <span class="hidden-sm hidden-xs">Wilgotność [%]</span>
                                        <span class="visible-sm visible-xs">W [%]</span>
                                    </th>
                                    <th class="hidden-sm hidden-xs">Czujnik</th>
                                    <th>Pomieszczenie</th>
                                    <th class="date-th">Data</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {
            $('#measurements').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'order'       : [[4, 'desc']],
                'info'        : true,
                'autoWidth'   : true
            })
        });

        var start = moment();
        var end = moment();

        function cb(start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'))
            $('#dateRange').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
        }

        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Dzisiaj'       : [moment(), moment()],
                    'Wczoraj'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ostatnie 7 dni' : [moment().subtract(6, 'days'), moment()],
                    'Ostatnie 30 dni': [moment().subtract(29, 'days'), moment()],
                    'Bieżący miesiąc'  : [moment().startOf('month'), moment().endOf('month')],
                    'Poprzedni miesiąc'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                locale: {
                    "customRangeLabel": "Inny przedział",
                    "applyLabel": 'Zatwierdź',
                    "cancelLabel": 'Anuluj'
                },
            },
            cb
        )

        cb(start, end)
    </script>
@endsection
