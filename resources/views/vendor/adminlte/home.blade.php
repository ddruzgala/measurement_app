@extends('adminlte::layouts.app')

@section('contentheader_title')
	Home
@endsection

@section('htmlheader_title')
	Home
@endsection

<style>
	@media (min-width: 1320px) {
		.measure-val {
			font-size: 32px !important;
			font-weight: bold;
		}

		.box-title {
			font-size: 28px !important;
			font-weight: bold;
		}
	}
	@media (max-width: 1319px) {
		.measure-val {
			font-size: 28px !important;
			font-weight: bold;
		}
		.box-title {
			font-size: 26px !important;
			font-weight: bold;
		}
	}

	@media (max-width: 345px) {
		.measure-val {
			font-size: 22px !important;
			font-weight: bold;
		}
		.box-title {
			font-size: 20px !important;
			font-weight: bold;
		}
	}
</style>
@section('main-content')


	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-lg-12  col-md-12 col-xs-12">

				{{--<!-- Default box -->--}}
				{{--<div class="box">--}}
					{{--<div class="box-header with-border">--}}
						{{--<h3 class="box-title">Admin Panel</h3>--}}

						{{--<div class="box-tools pull-right">--}}
							{{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
								{{--<i class="fa fa-minus"></i></button>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div class="box-body">--}}
						{{--Cześć, {{\Illuminate\Support\Facades\Auth::user()->name}}!, sprawdź temperaturę i wilgotność w domku :)--}}
					{{--</div>--}}
					{{--<!-- /.box-body -->--}}
				{{--</div>--}}
				{{--<!-- /.box -->--}}

				<!-- Small boxes (Stat box) -->
				<div class="row">
					@foreach($rooms as $room)
						<div class="col-lg-4 col-md-6 col-xs-12">
							<!-- small box -->
							<div class="small-box bg-green">
								<div class="inner" style="height: 178px">
									<p class="text-center text-bold box-title">{{$room->name}}</p>
									@if(empty($room->lastMeasurement()))
										<br>
										<p class="text-center">Brak pomiarów</p>
									@else
										<div class="row">
											<div class="col-lg-6 col-xs-6 text-center">
												<p class="measure-val">
													<i class="fa fa-thermometer-half" aria-hidden="true"></i> {{$room->lastMeasurement()->temperature}}°C
												</p>
											</div>
											<div class="col-lg-6 col-xs-6 text-center">
												<p class="measure-val">
													<i class="fa fa-tint" aria-hidden="true"></i> {{$room->lastMeasurement()->humidity}}%
												</p>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12 col xs-12 text-center">
												<h4>
													<i class="fa fa-clock-o" aria-hidden="true"></i>
													{{\Carbon\Carbon::parse($room->lastMeasurement()->created_at)->addHour()->format('H:i')}} &nbsp;&nbsp;
													<i class="fa fa-calendar" aria-hidden="true"></i>
													{{\Carbon\Carbon::parse($room->lastMeasurement()->created_at)->addHour()->format('Y-m-d')}}
												</h4>
											</div>
										</div>
									@endif
								</div>
								<div class="icon">
									<i class="ion ion-bag"></i>
								</div>
								<a href="/room/{{$room->id}}}" class="small-box-footer">Więcej informacji <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
					@endforeach
				</div>
			</div>
		</div>
	</div>

@endsection
