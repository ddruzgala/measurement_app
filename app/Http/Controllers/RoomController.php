<?php

namespace App\Http\Controllers;

use App\Room;
use App\Sensor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Auth::user()->rooms()->active()->with('sensor')->get();
        $sensors = Auth::user()->sensors()->with('room')->get();

        return view('adminlte::pages.rooms', compact('rooms', 'sensors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('sensor_id');
        $data['user_id'] = Auth::user()->id;
        $room = Room::create($data);
        $sensorId = $request->get('sensor_id');
        if(!empty($sensorId))
        {
            $oldSensor = $room->sensor()->first();
            if($oldSensor) {
                $oldSensor->room()->dissociate();
                $oldSensor->save();
            }

            $sensor = Sensor::find($sensorId);
            $sensor->room()->associate($room);
            $sensor->save();
        }

        return back();
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $room = Room::find($id);

        if(!$room->authorize())
        {
            return back();
        }

        $measurements = $room->measurements()->today()->get();
        $range = Carbon::today('Europe/Warsaw')->format('d/m/Y');

        if($request->has('date_range'))
        {
            $range = $request->get('date_range');
            $start = Carbon::parse(substr($range, 0, -13));
            $end = Carbon::parse(substr($range, -10))->endOfDay();

            if($start->toDateString() === $end->toDateString()) {
                $range = $start->format('d/m/Y');
            } else {
                $range = $start->format('d/m/Y').' - '.$end->format('d/m/Y');
            }

            $measurements = $room->measurements()
                ->where('created_at', '>=', $start->addHour(-1))
                ->where('created_at', '<=', $end->addHour(-1))
                ->get();

        }

        return view('adminlte::pages.room', compact('measurements', 'room', 'range'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::find($id);
        if(!$room->authorize())
        {
            return back();
        }

        $data = $request->all();
        if($request->has('sensor_id')) {
            $sensorId = $request->get('sensor_id');
            if(!empty($sensorId))
            {
                $oldSensor = $room->sensor()->first();
                if($oldSensor) {
                    $oldSensor->room()->dissociate();
                    $oldSensor->save();
                }

                $sensor = Sensor::find($sensorId);
                $sensor->room()->associate($room);
                $sensor->save();
            }

            $data = $request->except('sensor_id');
        }

        $room->update($data);
        $room->save();

        if($request->has('active')) {
            return response()->json([], 200);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);
        if(!$room->authorize())
        {
            return back();
        }

        $oldSensor = $room->sensor()->first();
        if($oldSensor) {
            $oldSensor->room()->dissociate();
            $oldSensor->save();
        }

        $room->delete();

        return back();
    }
}
