<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        $user = Auth::user();
        $rooms = $user->rooms()->get();
        $tempMin = $user->temp_min;
        $tempMax = $user->temp_max;
        $preferences = $user->notificationPreferences()->first();

        return view('adminlte::pages.settings', compact('rooms','tempMin', 'tempMax', 'preferences'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($request->has('temp_min') && $request->has('temp_max')) {
            $tempMin = $request->get('temp_min');
            $tempMax = $request->get('temp_max');
            if($tempMin <= $tempMax && $tempMin !== null && $tempMax !== null) {
                $user->temp_min = $request->get('temp_min');
                $user->temp_max = $request->get('temp_max');
                $user->save();
                return response()->json(['message' => 'Limity zaktualizowane!'], 200);
            } elseif($tempMin === null || $tempMax === null) {
                $user->temp_min = $request->get('temp_min');
                $user->temp_max = $request->get('temp_max');
                $user->save();
                return response()->json(['message' => 'Limity zaktualizowane!'], 200);
            }
            return response()->json(['message' => 'Temperatura maksymalna nie może być mniejsza od minimalnej!'], 422);

        }

        return back();
    }
}
