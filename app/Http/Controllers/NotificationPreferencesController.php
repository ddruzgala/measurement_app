<?php

namespace App\Http\Controllers;

use App\NotificationPreferences;
use Illuminate\Http\Request;

class NotificationPreferencesController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $preferences = NotificationPreferences::find($id);
        $preferences->email = $request->get('email') == 'true' ? true : false;
        $preferences->sms = $request->get('sms') == 'true' ? true : false;
        $preferences->save();

        return response()->json(['message' => 'Preferencje powiadomień zaktualizowane!'], 200);
    }
}
