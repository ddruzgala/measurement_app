<?php

namespace App\Http\Controllers;

use App\Measurement;
use App\Notifications\TemperatureDecreased;
use App\Notifications\TemperatureIncreased;
use App\Room;
use App\Sensor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeasurementController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $measurements = Auth::user()->measurements()
            ->where('measurements.created_at', '>=', Carbon::today('Europe/Warsaw')->startOfDay()->addHour(-1))
            ->where('measurements.created_at', '<=', Carbon::today('Europe/Warsaw')->endOfDay()->addHour(-1))
            ->get();

        $range = Carbon::today('Europe/Warsaw')->format('d/m/Y');

        if($request->has('date_range'))
        {
            $range = $request->get('date_range');
            $start = Carbon::parse(substr($range, 0, -13));
            $end = Carbon::parse(substr($range, -10))->endOfDay();

            if($start->toDateString() === $end->toDateString()) {
                $range = $start->format('d/m/Y');
            } else {
                $range = $start->format('d/m/Y').' - '.$end->format('d/m/Y');
            }

            $measurements = Auth::user()->measurements()
                ->where('measurements.created_at', '>=', $start->addHour(-1))
                ->where('measurements.created_at', '<=', $end->addHour(-1))
                ->get();
        }

        return view('adminlte::pages.measurement', compact('measurements', 'range'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =  $request->all();
        $sensor = Sensor::find($data['sensor_id']);
        $data['room_id'] = $sensor->room_id;

        $user = $sensor->user()->first();
        $temperature = (float)$data['temperature'];
        $room = Room::find($sensor->room_id);
        $lastTemperature = $room->lastMeasurement() ? $room->lastMeasurement()->temperature : null;
        $lastNotifiedTemperature = $room->lastNotifiedMeasurement() ? $room->lastNotifiedMeasurement()->temperature : null;
        $measurement = Measurement::create($data);

//        if(
//            ($user->temp_min !== null) && ($temperature <= $user->temp_min) &&
//            (
//                (($lastTemperature !== null) && ($lastTemperature > $user->temp_min)) ||
//                (($lastNotifiedTemperature !== null) && ($temperature <= $lastNotifiedTemperature - 0.5)) ||
//                ($lastTemperature === null) ||
//                ($lastNotifiedTemperature === null)
//            )
//        ) {
//            $user->notify(new TemperatureDecreased($sensor->room, $temperature));
//            $measurement->notified = true;
//            $measurement->save();
//        }
//
//        if(
//            ($user->temp_max !== null) && ($temperature >= $user->temp_max) &&
//            (
//                (($lastTemperature !== null) && ($lastTemperature < $user->temp_max)) ||
//                (($lastNotifiedTemperature !== null) && ($temperature >= $lastNotifiedTemperature + 0.5)) ||
//                ($lastTemperature === null) ||
//                ($lastNotifiedTemperature === null)
//            )
//        ) {
//            $user->notify(new TemperatureIncreased($sensor->room, $temperature));
//            $measurement->notified = true;
//            $measurement->save();
//        }

        if(($user->temp_min !== null) && ($temperature <= $user->temp_min)) {
            $user->notify(new TemperatureDecreased($sensor->room, $temperature));
            $measurement->notified = true;
            $measurement->save();
        }

        if(($user->temp_max !== null) && ($temperature >= $user->temp_max)) {
            $user->notify(new TemperatureIncreased($sensor->room, $temperature));
            $measurement->notified = true;
            $measurement->save();
        }

        return response()->json(['measurement' => $measurement]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
