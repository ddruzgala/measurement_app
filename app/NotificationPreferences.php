<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPreferences extends Model
{
    /**
     * @var array
     */
    protected  $fillable = [
      'user_id', 'email', 'sms'
    ];
}
