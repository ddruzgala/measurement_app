<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Room extends Model
{
    const WIATROLAP = 'Wiatrołap';
    const DUZY_POKOJ = 'Duży pokój';
    const KUCHNIA = 'Kuchnia';
    const POKOJ_GOSC_DOL = 'Pokój gościnny dół';
    const POKOJ_GOSC_GORA = 'Pokój gościnny góra';
    const GARAZ = 'Garaż';
    const SYPIALNIA = 'Sypialnia';
    const STRYCH = 'Strych';
    const KNIAZNINA = 'Kniaźnina';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'active', 'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sensor()
    {
        return $this->hasOne(Sensor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function measurements()
    {
        return $this->hasMany(Measurement::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * @return Model|null|static
     */
    public function lastMeasurement()
    {
        $lastMeasurement = $this->measurements()->latest()->first();
        return $lastMeasurement;
    }

    /**
     * @return mixed
     */
    public function lastNotifiedMeasurement()
    {
        $lastNotifiedMeasurement = $this->measurements()->notified()->latest()->first();
        return $lastNotifiedMeasurement;
    }

    /**
     * @return mixed
     */
    public function authorize()
    {
        return Room::where('id', $this->id)
            ->where('user_id', Auth::user()->id)->exists();
    }
}
