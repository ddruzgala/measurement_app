<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'temperature', 'humidity', 'room_id', 'sensor_id', 'notified'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sensor()
    {
        return $this->belongsTo(Sensor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotified($query)
    {
        return $query->where('notified', true);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeToday($query)
    {
        return $query
            ->where('created_at', '>=', Carbon::today('Europe/Warsaw')->startOfDay()->addHour(-1))
            ->where('created_at', '<=', Carbon::today('Europe/Warsaw')->endOfDay()->addHour(-1));
    }
}
