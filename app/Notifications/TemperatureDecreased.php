<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TemperatureDecreased extends Notification
{
    use Queueable;

    /**
     * TemperatureDecreased constructor.
     * @param $room
     * @param $temperature
     */
    public function __construct($room, $temperature)
    {
        $this->room = $room;
        $this->temperature = $temperature;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->prefersEmail() && $notifiable->prefersSms()) {
            return ['mail', 'nexmo'];
        }
        elseif($notifiable->prefersEmail()) {
            return ['mail'];
        }
        elseif($notifiable->prefersSms()) {
            return ['nexmo'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name = $this->room->user()->first()->name;
        return (new MailMessage)
            ->subject('Temperatura spadła!')
            ->line($this->room->name.': temperatura spadła do '.$this->temperature.'°C!')
            ->action('Sprawdź pomiary', url('/'))
            ->markdown('notifications.temperature', compact('name'));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage())
            ->content($this->room->name.': temperatura spadla do '.$this->temperature.'°C! ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
