<?php

use Illuminate\Database\Seeder;

class SensorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = \App\Room::all();
        $i = 1;
        foreach ($rooms as $room) {
            factory(App\Sensor::class)->create([
                'name' => 'Czujnik '.$i,
                'room_id' => $room->id
            ]);
            $i++;
        }
    }
}
