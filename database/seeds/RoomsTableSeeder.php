<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = [
            \App\Room::DUZY_POKOJ,
            \App\Room::GARAZ,
            \App\Room::KUCHNIA,
            \App\Room::POKOJ_GOSC_DOL,
            \App\Room::POKOJ_GOSC_GORA,
            \App\Room::SYPIALNIA,
            \App\Room::STRYCH,
            \App\Room::WIATROLAP,
            \App\Room::KNIAZNINA
        ];

        foreach ($rooms as $room) {
            factory(App\Room::class)->create([
                'name' => $room
            ]);
        }
    }
}
