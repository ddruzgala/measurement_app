<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('adminlte::auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::put('/user/{id}/update', 'UserController@update');
    Route::get('/settings', 'UserController@settings');

    Route::get('/measurement', 'MeasurementController@index')->name('measurement');

    Route::get('/room', 'RoomController@index');
    Route::post('/room', 'RoomController@store');
    Route::get('/room/{id}', 'RoomController@show');
    Route::put('/room/{id}/update', 'RoomController@update');
    Route::delete('/room/{id}/delete', 'RoomController@destroy');

//    Route::get('/sensor', 'SensorController@index');
//    Route::post('/sensor', 'SensorController@store');
//    Route::put('/sensor/{id}/update', 'SensorController@update');
//    Route::delete('/sensor/{id}/delete', 'SensorController@destroy');

    Route::put('/notification_preferences/{id}/update', 'NotificationPreferencesController@update');
});
